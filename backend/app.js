const express = require("express");
const mongoose = require("mongoose");
const config = require("./config.json");

const app = express();
const dbConnectinon = mongoose.connect(config.dbAddress, { useNewUrlParser: true });

require("./lib/stream.js").start();

app.listen(config.port);