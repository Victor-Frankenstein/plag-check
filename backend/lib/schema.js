const mongoose = require('mongoose');

const plag = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user: {
    type: String,
    required: true
  },
  permlink: {
    type: String,
    required: true
  },
  similarTo: {
    type: Array,
    required: true
  }
});

module.exports = mongoose.model('Plagiarised', plag);