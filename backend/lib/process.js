const hooke = require("hookejs");
const config = require("../config.json");

function process(trx) {
  //TODO: Refactor, need seperate for hooke 
  const ops = trx.operations;
  for (let i in ops) {
    const op = ops[i];
    if (op[0] === "comment") {
      const comment = op[1];
      const author = comment.author;
      const permlink = comment.permlink;
      const body = comment.body;
      hooke.matchPrint({ apikey: config.apiKey, text: body }).then((res) => {
        /*
        EXAMPLE:



Comparison



FROM https://www.choosemyplate.gov/ten-tips-make-better-food-choices


ORIGINAL: personalized nutrition information based on your age, gender, height, weight, and physical activity level. The MyPlate Plan provides your calorie level and shows how much you should eat from each of the five food groups. 
  
  Enjoy your food but eat less 
  Use a smaller plate at meals to help control the amount of food and calories you eat. Take time to enjoy smaller amounts of food.
  
  Strengthen your bones 
  Choose foods like fat-free and low-fat milk, cheese, yogurt, and fortified soymilk to help strengthen bones. Be sure your morning coffee includes fat-free or low-fat milk.


COMPARED: personalized nutrition information based on your age, gender, height, weight, and physical activity level. The  MyPlate Plan  provides your calorie level and shows how much you should eat from each of the five food groups.  
   
   
    Enjoy your food but eat less   
  Use a smaller plate at meals to help control the amount of food and calories you eat. Take time to enjoy smaller amounts of food. 
   
   
    Strengthen your bones   
  Choose foods like fat-free and low-fat milk, cheese, yogurt, and fortified soymilk to help strengthen bones. Be sure your morning coffee includes fat-free or low-fat milk.


SCORE: 49



FROM https://www.choosemyplate.gov/resources/MyPlatePlan


ORIGINAL: based on your age, gender, height, weight, and physical activity level. The MyPlate Plan


COMPARED: based on your age, sex, height, weight, and physical activity level. The MyPlate Plan is also available in  Spanish . 

    

  
Get the  MyPlate Plan


SCORE: 8



FROM https://www.choosemyplate.gov/node/5799


ORIGINAL: based on your age, gender, height, weight, and physical activity level. The MyPlate Plan


COMPARED: based on your age, sex, height, weight, and physical activity level.   

        The MyPlate Plan


SCORE: 6.125



FROM https://www.choosemyplate.gov/start-simple-myplate


ORIGINAL: height, weight, and physical activity level


COMPARED: height, weight, and physical activity level. You can even see your physical activity


SCORE: 8.333333333333334



FROM https://courses.lumenlearning.com/suny-fmcc-fitness/chapter/dietary-guidelines-young-and-middle-adulthood/


ORIGINAL: smaller plate at meals to help control the amount of food and calories you eat. Take time to enjoy smaller amounts of food.



COMPARED: smaller plate at meals to help control the amount of food and calories you eat. Take time to enjoy smaller amounts of food.


SCORE: 12.1



FROM https://courses.lumenlearning.com/suny-fmcc-fitness/chapter/dietary-guidelines-young-and-middle-adulthood/


ORIGINAL: Choose foods like fat-free and low-fat milk, cheese, yogurt, and fortified soymilk to help strengthen bones. Be sure your morning coffee includes fat-free or low-fat milk.


COMPARED: Choose foods like fat-free and low-fat milk, cheese, yogurt, and fortified soymilk to help strengthen bones. Be sure your morning coffee includes fat-free or low-fat milk.


SCORE: 17.066666666666666

*/
        console.log(res)
      })
    }
  }
}


module.exports = {
  process
}